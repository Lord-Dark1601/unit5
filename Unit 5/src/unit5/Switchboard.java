package unit5;

public class Switchboard {

	private Call[] calls;
	private int numCall;

	public Switchboard(int numMaxCalls) {
		calls = new Call[numMaxCalls];
		numCall = 0;
	}

	public void registerCall(Call call) {
		if (numCall >= calls.length) {
			System.err.println("Exceeded max number of calls");
		} else {
			calls[numCall] = call;
			numCall++;
		}
	}

	public void printCalls() {
		for(int i=0; i<numCall; i++) {
			System.out.println(calls[i]);
		}
	}
	
	public int numRegisteredCalls() {
		return numCall;
	}
	
	public double getTurnover() {
		double count = 0.0;
		for(int i=0; i<numCall; i++) {
			count += calls[i].getCost();
		}
		return count;
	}
}

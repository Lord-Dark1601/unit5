package unit5;

public class TestBook {

	public static void main(String[] args) {

		Author a = new Author("Richard Rothfuss", "richardr@gmail.com", 'm');
		Book b = new Book("The name of the wind", a, 29.9, 10);
		Book b2 = new Book("The nootbook", new Author("PatrickSmith", "ppp@gmail.com", 'm'), 24.9, 5);
		Book b3 = new Book("The fear of a wise man", a, 34.9, 7);
		System.out.println(b.toString());

		Book[] books;
		books = new Book[5];
		books[0] = b;
		books[1] = b2;
		books[2] = b3;
		books[3] = new Book("Don Quijote de la Mancha", new Author("Miguel de Cervantes", "", 'm'), 39.9, 50);
		books[4] = new Book("It", new Author("Sthepen king", "TitoSthipy@gmail.com", 'm'), 24.9, 12);

		System.out.println("-----------------");
		for (int i = 0; i < books.length; i++) {
			System.out.println(books[i].toString());
		}
		System.out.println("-----------------");
		for (Book book : books) {
			System.out.println(book);
		}
	}

}

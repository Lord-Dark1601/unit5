package unit5;

import java.util.*;

public class Ticket {

	private Block[] ticket;

	public Ticket(int numBlocks) {
		ticket = new Block[numBlocks];
		for (int i = 0; i < numBlocks; i++) {
			Block b = new Block(6);
			ticket[i] = b;
		}

	}

	public Ticket(boolean full) {
		this(1);
		if (full) {
			ticket = new Block[8];
			Drum d = new Drum();
			for (int i = 0; i < ticket.length; i++) {
				int[] nums = new int[6];
				for (int j = 0; j < nums.length; j++) {
					nums[j] = d.extractBall();
				}
				ticket[i] = new Block(nums);
			}
		}
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < ticket.length; i++) {
			s += ticket[i].toString() + "\n";
		}
		return s;
	}

	public void printUsedNumbers() {
		int[] nums = new int[50];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = 0;
		}
		for (int i = 0; i < ticket.length; i++) {
			for (int j = 0; j < ticket[0].getNumbers().length; j++) {
				nums[ticket[i].getNumbers()[j]] = ticket[i].getNumbers()[j];
			}
		}
		Arrays.sort(nums);
		boolean first = true;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != 0) {
				if (first) {
					System.out.print(nums[i]);
					first = false;
				} else {
					System.out.print(", " + nums[i]);
				}
			}

		}
		System.out.println();
	}
}

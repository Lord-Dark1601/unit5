package unit5;

import java.util.*;

public class Block {

	private int[] numbers;

	public Block(int numNumbers) {
		int counter = 0;
		if (numNumbers < 6) {
			numNumbers = 6;
		} else if (numNumbers > 49) {
			numNumbers = 49;
		}
		numbers = new int[numNumbers];
		while (counter < numNumbers) {
			int x = (int) (Math.random() * 49 + 1);
			boolean repeat = false;
			for (int i = 0; i < counter; i++) {
				if (numbers[i] == x) {
					repeat = true;
					break;
				}
			}
			if (!repeat) {
				numbers[counter] = x;
				counter++;
			}
		}
		Arrays.sort(numbers);
	}

	public Block(int... numbers) {
		this.numbers = numbers;
		Arrays.sort(numbers);
	}
	public int[] getNumbers() {
		return numbers;
	}

	@Override
	public String toString() {
		String s = "" + numbers[0];
		for (int i = 1; i < numbers.length; i++) {
			s += ", " + numbers[i];
		}
		return s;
	}

}

package unit5;

public class MainClass4 {

	public static void main(String[] args) {
		Rectangle r = new Rectangle();
		System.out.println(r);
		r.moveTo(4, 4);
		System.out.println(r);

		Rectangle r2 = new Rectangle(new Point(1, 1), 8, 6);
		System.out.println(r2 + "  " + r2.getArea());
		Rectangle r3 = new Rectangle(new Point(1, 1), new Point(4, 2));
		System.out.println(r3 + "  " + r3.getArea());

		Rectangle ex40 = new Rectangle(new Point(5, 6), 10, 8);
		System.out.println(ex40);
		System.out.println("Top-Left: " + ex40.getTopLeftPoint() + "\nTop-Right: " + ex40.getTopRightPoint()
				+ "\nBottom-Left: " + ex40.getBottomLeftPoint() + "\nBottom-Right: " + ex40.getBottomRightPoint());
	}

}

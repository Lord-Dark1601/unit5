package unit5;

public class Segment {

	private Point startPoint;
	private Point endPoint;

	public Segment() {
		startPoint = new Point();
		endPoint = new Point();
	}

	public Segment(Point startP, Point endP) {
		startPoint = startP;
		endPoint = endP;
	}

	public Point getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}

	public Point getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}

	public double module() {
		int x = endPoint.getX() - startPoint.getX();
		int y = endPoint.getY() - startPoint.getY();
		double distance = Math.sqrt(x * x + y * y);
		return distance;
	}

	@Override
	public String toString() {
		return "(" + startPoint.getX() + ", " + startPoint.getY() + ") - " + "(" + endPoint.getX() + ", "
				+ endPoint.getY() + ")";
	}

	public void setOffset(int offX, int offY) {
		startPoint.setOffset(offX, offY);
		endPoint.setOffset(offX, offY);
	}

}

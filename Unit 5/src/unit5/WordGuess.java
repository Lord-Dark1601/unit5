package unit5;
import java.util.Scanner;

public class WordGuess {

	public static void main(String[] args) {

		MagicWord mW = new MagicWord();
		Scanner input = new Scanner(System.in);
		String inputLetter;
		
		while (!mW.isGameOver()) {
			System.out.println(mW.toPrintLetters());
			System.out.print("Enter your letter: ");
			inputLetter= input.next();
			mW.checkLetter(inputLetter);
			//System.out.println(mW.toPrintLetters());
			System.out.println(mW.checkGameOver());
			
		}
		input.close();
	}

}

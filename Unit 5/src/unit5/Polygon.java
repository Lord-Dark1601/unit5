package unit5;

public class Polygon {

	private Point[] points;

	public Polygon(Point[] points) {
		this.points = points;
	}

	public Polygon() {
		points = new Point[0];
	}

	@Override
	public String toString() {
		String s = points[0].toString();
		for (int i = 1; i < points.length; i++) {
			s += " - " + points[i].toString();
		}
		s += " - " + points[0].toString();
		return s;
	}

	public void setOffset(int offX, int offY) {
		for (int i = 0; i < points.length; i++) {
			points[i].setOffset(offX, offY);
		}
	}

	public double getLength() {
		double per = 0.0;
		Point p = points[0];
		for (int i = 1; i < points.length; i++) {
			// Segment s=new Segment(p, points[i]);
			per += new Segment(p, points[i]).module();
			p = points[i];
		}
		per += new Segment(p, points[0]).module();
		return per;
	}

}

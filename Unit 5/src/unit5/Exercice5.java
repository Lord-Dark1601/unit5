package unit5;

public class Exercice5 {

	public static void main(String[] args) {

		Switchboard sb = new Switchboard(20);
		sb.registerCall(new Call(111, 333, 15, 1, false));
		sb.registerCall(new Call(222, 444, 12, 1, false));
		sb.registerCall(new Call(555, 666, 20, 2, false));

		sb.printCalls();
		System.out.println("-------------");
		System.out.println("Num Calls: " + sb.numRegisteredCalls());
		System.out.println("Turnover: " + sb.getTurnover());
	}

}

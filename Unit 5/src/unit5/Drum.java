package unit5;

public class Drum {

	private int[] numbers;
	private int count = 0;

	public Drum() {
		numbers = new int[49];
		for (int i = 1; i <= 49; i++) {
			numbers[i - 1] = i;
		}
		shuffleDrum();
	}

	public int extractBall() {
		int x = numbers[count];
		count++;
		return x;
	}

	private void shuffleDrum() {
		for (int i = 0; i < numbers.length * 10; i++) {
			int x = (int) (Math.random() * numbers.length);
			int y = (int) (Math.random() * numbers.length);
			int c = numbers[y];
			numbers[y] = numbers[x];
			numbers[x] = c;
		}
	}

}

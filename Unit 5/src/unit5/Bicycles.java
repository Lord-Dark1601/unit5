package unit5;

public class Bicycles {
	private int cadence;
	private int speed;
	private int gear;
	
	public Bicycles() {
		cadence=0;
		speed=0;
		gear=5;
	}

	public Bicycles(int cadence, int speed, int gear) {
		this.cadence = cadence;
		this.speed = speed;
		this.gear = gear;
	}

	void changeCadence(int newValue) {
		cadence = newValue;
	}

	void changeGear(int newValue) {
		gear = newValue;
	}

	void changeSpeedUp(int increment) {
		speed = speed + increment;
	}

	void applyBrakes(int decrement) {
		speed = speed - decrement;
		if (speed < 0) {
			speed = 0;
		}
	}

	void printStates() {
		System.out.println("cadence: " + cadence + " speed: " + speed + " gear: " + gear);
	}
}

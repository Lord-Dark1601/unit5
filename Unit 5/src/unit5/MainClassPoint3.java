package unit5;

public class MainClassPoint3 {

	public static void main(String[] args) {
		Point p1 = new Point(4, 5);
		Point p2 = new Point(6, 8);
		Segment s = new Segment(p1, p2);
		s.setOffset(4, 4);
		System.out.println(s.toString());
		System.out.println(s.module());
	}

}

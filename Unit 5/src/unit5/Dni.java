package unit5;

public class Dni {

	private int number;
	private char letter;
	private static char[] LETTERS = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V',
			'H', 'L', 'C', 'K', 'E' };

	public Dni() {
		number = 00000000;
		letter = 'Ñ';
	}

	public Dni(int number, char letter) {
		this.number = number;
		this.letter = letter;
		if (letter != LETTERS[number % 23]) {
			this.number = -number;
		}
	}

	public Dni(int numer) {
		this.number = numer;
		letter = LETTERS[number % 23];

	}

	public Dni(String s) {
		letter = s.charAt(s.length() - 1);
		s = s.replaceAll("[^\\d.]", "");
		number = Integer.valueOf(s);

	}

	public int getNumber() {
		return number;
	}

	public char getLetter() {
		return letter;
	}

	public void setNumber(int number) {
		this.number = number;
		if (letter != LETTERS[number % 23]) {
			this.number = -number;
		}
	}

	public void checkLetter() {
		if (number < 0) {
			if (LETTERS[number % 23] != letter) {
				number = -number;
			}
		} else {
			if (LETTERS[-number % 23] != letter) {
				number = -number;
			}
		}

	}

	public boolean isCorrect() {
		if (number >= 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "" + number + letter;
	}

	public String toFormattedString() {
		StringBuilder s = new StringBuilder (String.valueOf(number));
		if(number>999) {
			s.insert(s.length()-3, '.');
		}
		if(number>999999) {
			s.insert(s.length()-7, '.');
		}
		if(number>999999999) {
			s.insert(s.length()-11, '.');
		}
		return s.toString()+"-"+letter;
	}
	
	public boolean correctDni() {
		if (number<0) {
			return false;
		}else {
			return true;
		}
	}
	
	public static char letterForDni( int number) {
		return LETTERS[number%23];
	}
	
	public static String NifForDni(int number) {
		return new Dni(number).toString();
	}
}

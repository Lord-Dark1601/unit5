package unit5;

import java.io.*;
import java.util.*;

public class MyPolynomial {

	private double[] coeffs;

	public MyPolynomial(double... coeffs) {
		this.coeffs = coeffs;
	}

	public MyPolynomial(int degree) {
		for (int i = 0; i <= degree; i++) {
			coeffs[i] = 0.0;
		}
	}

	public MyPolynomial(String filename) {
		Scanner in = null;
		try {
			in = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int degree = in.nextInt();
		coeffs = new double[degree + 1];
		for (int i = 0; i < coeffs.length; i++) {
			coeffs[i] = in.nextDouble();
		}
	}

	public int getDegree() {
		return coeffs.length - 1;
	}

	public String toString() {
		String s = "";
		int degree = getDegree();
		for (int i = 0; i < coeffs.length; i++) {
			s += coeffs[i] + "x^" + degree;
			if (i != coeffs.length - 1) {
				s += "+";
			}
			degree--;
		}
		return s;
	}

	public double evaluate(double x) {
		double result = 0;
		;
		for (int i = 0; i < coeffs.length; i++) {
			result += coeffs[i] * Math.pow(x, i);
		}
		return result;
	}

	public MyPolynomial add(MyPolynomial poly) {
		int length1 = coeffs.length;
		int length2 = poly.coeffs.length;
		if (length1 < length2) {
			for (int i = 0; i < length1; i++) {
				poly.coeffs[i] += coeffs[i];
			}
		} else {
			for (int i = 0; i < length2; i++) {
				coeffs[i] += poly.coeffs[i];
			}
		}
		return poly;
	}

	public MyPolynomial multiply(MyPolynomial poly) {
		double[] result = new double[poly.getDegree() + coeffs.length - 1];
		int degree = 0;
		for (int i = 0; i <= poly.getDegree(); i++) {
			for (int j = 0; j < coeffs.length; j++) {
				degree = i + j;
				result[degree] += poly.coeffs[i] * coeffs[j];
			}
		}

		return new MyPolynomial(result);
	}

	public String toStringProfe() {
		String s = "";
		String term;
		for (int i = 0; i < coeffs.length; i++) {
			if (coeffs[i] != 0) {
				if (i == 0) {
					term = "" + coeffs[i];
				} else {
					if (i == 1) {
						term = coeffs[i] + "x";
					} else {
						term = coeffs[i] + "x^" + i;
					}
				}
				if (s.length() != 0) {
					s = term + "+" + s;
				} else {
					s = term + s;
				}
			}

		}
		return s;
	}

	public MyPolynomial addProfe(MyPolynomial poly) {
		double[] result, biggest, smallest;
		if (coeffs.length > poly.coeffs.length) {
			biggest = coeffs;
			smallest = poly.coeffs;
		} else {
			biggest = poly.coeffs;
			smallest = coeffs;
		}
		result = new double[biggest.length];
		for (int i = 0; i < biggest.length; i++) {
			result[i] = biggest[i];
		}
		for (int i = 0; i < smallest.length; i++) {
			result[i] += smallest[i];
		}
		return new MyPolynomial(result);
	}

	public MyPolynomial multiplyProfe(MyPolynomial poly) {
		double[] result = new double[poly.getDegree() + coeffs.length - 1];
		for (int i = 0; i < result.length; i++) {
			result[i] = 0;
		}
		for (int i = 0; i < coeffs.length; i++) {
			for (int j = 0; j < poly.getDegree(); j++) {
				result[i + j] += coeffs[i] * poly.coeffs[j];
			}
		}
		return new MyPolynomial(result);
	}
}

package unit5;

public class Rectangle extends Point {

	private int width;
	private int heidht;

	public Rectangle() {
		super();
		width = 0;
		heidht = 0;
	}

	public Rectangle(Point p1, int width, int height) {
		super(p1.getX(), p1.getY());
		this.width = width;
		this.heidht = height;
	}

	public Rectangle(Point p1, Point p2) {
		super(p1.getX(), p1.getY());
		width = p2.getX() - p1.getX();
		heidht = p2.getY() - p1.getY();
	}

	public Rectangle(Segment segment) {
		super(segment.getStartPoint().getX(), segment.getStartPoint().getY());
		Point p1 = segment.getStartPoint();
		Point p2 = segment.getEndPoint();
		width = p2.getX() - p1.getX();
		heidht = p2.getY() - p1.getY();
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeidht() {
		return heidht;
	}

	public void setHeidht(int heidht) {
		this.heidht = heidht;
	}

	public double getArea() {
		double area = 0.0;
		area = width * heidht;
		return area;
	}

	public Point getPoint() {
		return new Point(getX(), getY());
	}

	public Point getTopLeftPoint() {
		return new Point(getX(), getY() + heidht);
	}

	public Point getTopRightPoint() {
		return new Point(getX() + width, getY());
	}

	public Point getBottomLeftPoint() {
		return new Point(getX(), getY());
	}

	public Point getBottomRightPoint() {
		return new Point(getX() + width, getY() + heidht);
	}

	@Override
	public String toString() {
		return super.toString() + "  width: " + width + "  heidht: " + heidht;
	}

}

package unit5;

public class MainDNI {

	public static void main(String[] args) {
		Dni d = new Dni("53786695E");
		System.out.println("number: " + d.getNumber() + " || Letter: " + d.getLetter());
		
		Dni d2 = new Dni (53786695);
		System.out.println(d2.toString());
		System.out.println(d2.toFormattedString());
		
	}

}

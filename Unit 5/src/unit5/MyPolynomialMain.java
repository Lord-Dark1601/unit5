package unit5;

public class MyPolynomialMain {

	public static void main(String[] args) {

		MyPolynomial p1 = new MyPolynomial(1.1, 2.2, 3.3);
		MyPolynomial p2 = new MyPolynomial(1.1, 2.2, 3.3, 4.4, 5.5);

		double[] coeffs = { 1.2, 3.4, 5.6, 7.8 };
		MyPolynomial p3 = new MyPolynomial(coeffs);
		MyPolynomial p4 = new MyPolynomial("poly");
		MyPolynomial p11 = new MyPolynomial(1, 1, 1);
		System.out.println(p11.add(p11).toStringProfe());
		
		
		System.out.println(p11.multiply(p11).toStringProfe());
		

	}

}

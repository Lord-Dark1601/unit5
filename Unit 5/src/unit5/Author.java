package unit5;

import java.util.*;

public class Author {

	private String name;
	private String email;
	private char gender;

	public Author(String name, String email, char gender) {
		this.name = name;
		this.email = email;
		this.gender = gender;
	}

	public static Author createAuthorFromKeyboard(Scanner input) {
		String name;
		String email;
		char gender;
		System.out.println("Author name?");
		name = input.next();
		System.out.println("Author email?");
		email = input.next();
		System.out.println("Author gender?");
		gender = input.next().charAt(0);

		Author a = new Author(name, email, gender);
		return a;

	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public char getGender() {
		return gender;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return name + " (" + gender + ") at " + email;
	}
}

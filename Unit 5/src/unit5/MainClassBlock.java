package unit5;

public class MainClassBlock {

	public static void main(String[] args) {

		Block b = new Block(10);
		System.out.println(b.toString());

		Ticket t = new Ticket(3);
		System.out.println(t.toString());
		t.printUsedNumbers();
		System.out.println("--------------------------------");
		Ticket t2 = new Ticket(true);
		System.out.println(t2.toString());
	}

}

package unit5;

public class MagicWord {

	private static final String[] WORDS = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
			"ten", "eleven", "twelve", "thirteen", "fouteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen",
			"twenty" };
	private boolean[] letters;
	private String word;
	private int selectWord;
	private int lengthLetter;
	private int attemps;
	public boolean gameOver;

	public MagicWord() {
		selectWord = (int) (Math.random() * WORDS.length);
		word = WORDS[selectWord];
		lengthLetter = 0;
		attemps = -1;
		gameOver = false;
		initLetters();
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void initLetters() {
		letters = new boolean[word.length()];
		for (int i = 0; i < letters.length; i++) {
			letters[i] = false;
		}
	}

	public void checkLetter(String letter) {
		lengthLetter = letter.length();
		if (lengthLetter == 1) {
			for (int i = 0; i < letters.length; i++) {
				if (letter.equalsIgnoreCase(word.substring(i, i + 1))) {
					letters[i] = true;
				}
			}
		} else if (lengthLetter > 1) {
			if (letter.equalsIgnoreCase(word)) {
				gameOver = true;
			}
		} else {
			System.err.println("No has pasado ninguna letra");
		}
	}

	public String toPrintLetters() {
		String s = "";
		attemps++;
		if (attemps <= 0) {
			System.out.print("Start word length: ");
			for (int i = 0; i < letters.length; i++) {
				s += (" _");
			}
		} else {
			System.out.print("Attemps " + attemps + ": ");
			for (int i = 0; i < letters.length; i++) {
				if (letters[i]) {
					s += (" ")+(word.substring(i, i + 1));
				} else {
					s += (" _");
				}
			}
		}
		return s;
	}

	public String checkGameOver() {
		String s = "";
		if (gameOver) {
			s += "Attemps " + attemps + ": " + word;
			s += "\nCONGRATULATION" + "\nYou got in " + attemps + " attempts";
		}
		return s;
	}
}
